pisa (3.0.33-2) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 10:13:04 +0100

pisa (3.0.33-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Refresh patches for new upstream release.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 23 Dec 2016 21:48:15 +0100

pisa (3.0.32-4) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Luciano Bello ]
  * Removing dependency to python-pypdf in favor of python-pypdf2.
    (Closes: #763981)
  * Standards-Version updated.

 -- Luciano Bello <luciano@debian.org>  Thu, 09 Jun 2016 15:27:11 +0200

pisa (3.0.32-3) unstable; urgency=low

  * Team upload.

  [ Piotr Ożarowski ]
  * Bump minimum required python-reportlab version to 2.2
    (see sx/pisa3/__init__.py)

  [ Matthias Schmitz ]
  * Switch to debhelper 9
  * Switch to source format 3.0 (quilt), rework patches from dpatch to quilt
  * Bump Standards version to 3.9.5
  * Add patch to fix wrong version detection of reportlab.
    (Closes: #571120)
  * To please lintian: re-word the manpage,
    add a patch to fix wrong interpreter path in examples,
    remove deprecated XB-Python-Version:,
    substitute python-dev by python in Build-Depends
  * Add python-pkg-resources to Depends: (Closes: #587221)

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * Remove long useless debian/pycompat.
  * debian/watch: rewrite using pypi.debian.net redirector.
  * debian/control:
    + switch dependecy on python-support to dh-python.  Closes: #786222
    + Bump standards-versions to 3.9.6, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 14 Dec 2015 00:57:40 +0000

pisa (3.0.32-2) unstable; urgency=medium

  * Team upload.
  * Add debian/patches/02-reportlab_bigger_than_two.patch.dpatch to fix
    reportlab version compatibility problems (Closes: #571120)
    - Thanks to Walter Doekes and Jamie McClelland for patches - both were
      fine patches, but the one used was the smaller change since Debian is in
      pre-release freeze

 -- Scott Kitterman <scott@kitterman.com>  Sun, 23 Nov 2014 21:51:06 -0500

pisa (3.0.32-1) unstable; urgency=low

  * Initial release (Closes: #504277). Thanks to Toby Smithe
    <tsmithe@ubuntu.com> for preparing the package. Thanks to Thomas
    Bechtold for helping with the licenses. And, yes, the file
    demo/tgpisa/tgpisa/release.py is just an example, not necessarily
    licensed under MIT license. CODE2000.TTF removed from source,
    package is under GPL-2.

 -- W. Martin Borgert <debacle@debian.org>  Mon, 08 Feb 2010 22:02:40 +0000
